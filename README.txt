RESOURCE BRIDGE

BACKGROUND
This module is designed for use only at the University of Wisconsin-Madison. It provides an XML "feed" for the
UW Portal.  This information will allow the portal to place links to Moodle courses on your students' "Academics"
page in the portal.  In this way students will be able to access their Moodle courses as easily as their Learn@UW
courses.  This module has been developed as a Moodle report.  It's a bit different from other Moodle reports  in that
it generates XML data instead of a human-friendly HTML page.  Still, it is basically a report, and most browsers
will render the XML in a reasonably human-friendly way.

INSTANCE SPECIFIC DATA
To get started, you will need to contact the UW Portal folks and agree on some data specific to your Moodle instance.
These are:
    Payload Type: specifies the kind of data contained in the XML feed
    Resource Type: specifies what kind of resource this data describes
    Token Type: additional specification of the kind of resource being described
    Supplier Id: This will identify the entity supplying the data (your UW college or department)
    Supplier: A "human friendly" name for your UW entity
In addition, you will need to provide a policy regarding use of the data you are supplying.  This should
be at a fixed web address.  This is referred to as your "proviso".

INSTALLATION
Once you have agreement with the Portal folks on these data, you should extract the code in the "admin/report"
directory in your Moodle site.  Since the report does not use any custom database tables, you don't need to go to
your "Notifications" page to initialize it.  However, you will need to configure the module before it becomes
operational.  You can access the configuration page by clicking on the "Reports -> Resource Bridge -> Settings" link
in your Site Administration tree.  There you can enable the report and type in the configuration data you defined
above.  Once the module has been configured, you can view its output by clicking on the "Reports -> Resource Bridge ->
XML Feed" link in your Site Administration tree.  NOTE: The feed link will only appear once you have enabled the feed.
When you are satisfied with the output, you can ask the Portal folks to begin accessing your data at:

                        "{MOODLE_ROOT}/report/resourcebridge/index.php"

CONTACTS
Contact information tends to get out of date quickly.  This information is current as of 12/16/2010.
UW Portal - Mike Farnham <mfarnham@doit.wisc.edu>
This module - Mike Litzkow <mlitzkow@wisc.edu>