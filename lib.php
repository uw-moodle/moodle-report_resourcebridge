<?php
/******************************************************************************
* UW Resource Bridge Report - Main Library
*
* Retrieves database information for the Resource Bridge XML feed, which serves
* links to Moodle courses.
*
* Author: Mike Litzkow/Nick Koeppen
******************************************************************************/

// Pull data needed by the resoure bridge from the database
function get_bridge_data( $term, $dept=NULL ) {
    global $DB;

    $table_coursemap = "{enrol_wisc_coursemap}";
    $table_course = "{course}";

    // Build SQL query
    $fields = array('id','subject_code','catalog_number','term','session_code','section_number','courseid');
    foreach($fields as $key => $field){
        $fields[$key] = "$table_coursemap.$field";
    }
    $columns = "distinct ".join($fields,', ');
    $tables = "$table_coursemap JOIN $table_course";

    if( empty($dept) ) {
        $where = "$table_coursemap.term = ?";
        $params = array($term);
    } else {
        $where = "$table_coursemap.term = ? AND subject_code = ?";
        $params = array($term, $dept);
    }

    for($i=1;$i<=5;$i++){
        $ordFields[] = $fields[$i];
    }
    $order = join($ordFields,', ');

    $query = "SELECT $columns FROM $tables WHERE $where ORDER BY $order";
    $result = $DB->get_records_sql($query, $params);

    return $result;
 }

 // Format the data into XML accepted by the resource bridge
 function xml_format( $data ) {
    global $CFG;
    $config = get_config("report_resourcebridge");

    $payload_type = $config->payloadtype;
    $resource_type = $config->resourcetype;
    $token_type = $config->tokentype;
    $supplier_id = $config->supplierid;
    $supplier = $config->supplier;
    $proviso_url = $config->provisoURL;

    $data_type = 'URI';
    $uri_base = $CFG->wwwroot.'/course/view.php?id=';
    $date = date(DATE_W3C);

    $xml_data = <<<END_OF_HEADER
<?xml version="1.0"  encoding="UTF-8" ?>
<!DOCTYPE ira SYSTEM "http://www.doit.wisc.edu/myuw/dtd/iraCourseResourceHarvest20.dtd">
<ira>

    <containerSupplier>
        <supplier>$supplier</supplier>
        <date scheme="W3C-DTF">$date</date>
        <proviso>$proviso_url</proviso>
    </containerSupplier>

    <resourceSupplier resourceSupplierId="$supplier_id">
        <supplier>$supplier</supplier>
        <date scheme="W3C-DTF">$date</date>
        <proviso>http://www.engr.wisc.edu/termsOfUse.html</proviso>
    </resourceSupplier>

END_OF_HEADER;

    if( !empty($data) ) {
        $xml_data .= "    <courseList>\r\n";
    }

    foreach( array_keys($data) as $id ) {
        $section = $data[$id];
        $xml_data .= <<<END_OF_COURSE
        <course>
            <subject>
                <subjectCode>$section->subject_code</subjectCode>
            </subject>
            <catalogNo>$section->catalog_number</catalogNo>
            <term>
                <termCode>$section->term</termCode>
                <session>
                    <sessionCode>$section->session_code</sessionCode>
                    <section>
                        <sectionCode>$section->section_number</sectionCode>
                        <payloadType type="$payload_type">
                            <resource type="$resource_type" resourceSupplierId="$supplier_id">
                                <data type="$data_type">$uri_base$section->courseid</data>
                                <resourceExternalToken type="$token_type">$section->courseid</resourceExternalToken>
                            </resource>
                        </payloadType>
                        </section>
                </session>
            </term>
        </course>

END_OF_COURSE;
    }

    if( !empty($data) ) {
        $xml_data .= "    </courseList>\r\n";
    }

    $xml_data .= "</ira>";
    return $xml_data;
}



// Generate the 4-digit code representing the current semester
function current_semester_code() {
    // Timetable DB encodes term as 4 digit code in form 'CYYT'
    // 'C' is  '0' for the 20'th century and '1' for the 21'st.
    // 'YY' is the last two digits of the year.
    // 'T' is '2' for Fall, '4' for Spring, and '6' for Summer
    // Here we figure out what the current term is.
    $now = getdate();
    if( $now['year'] >= 2000 ) {
        $century = 1;
    } else {
        $century = 0;
    }
    if( $now['mon'] >= 9 ) {
        $year = ($now['year'] + 1) % 100;
    } else {
        $year = $now['year'] % 100;
    }
    if( $year < 10 ) {
        $year = "0" . $year;
    }
    if( $now['mon'] < 6 ) {
        $term = 4;  // Jan - May = Spring
    } else if( $now['mon'] < 9 ) {
        $term = 6;  // Jun - Aug = Summer
    } else {
        $term = 2;  // Sep - Dec = Fall
    }
    return $century . $year . $term;
}

// This function returns XML as a string
function get_raw_xml( $term = null, $dept = null) {
    if( $term == null ) {
        $term = current_semester_code();
    }

    $data = get_bridge_data($term, $dept);
    $xml = xml_format( $data );

    return $xml;
}