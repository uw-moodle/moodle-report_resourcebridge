<?php
/******************************************************************************
* UW Resource Bridge Report - Global Settings
*
* Global (site-wide) configuration settings for Resource Bridge report.
*
* Author: Mike Litzkow/Nick Koeppen
******************************************************************************/
defined('MOODLE_INTERNAL') || die;

$modname = 'report_resourcebridge';

if (!$hassiteconfig) {
    return;        //Only applicable at the site level
}

/* =============== REPORT PAGES =============== */
/* Display XML feed link only if enabled */
$feedenabled = get_config($modname, 'enable');
if(!empty($feedenabled)){
    $url = new moodle_url('/report/resourcebridge/index.php');
    $ADMIN->add('reports', new admin_externalpage('reportresourcebridgefeed', get_string('resourcebridge', $modname), $url->out()));
}
/* =============== REPORT PAGES =============== */

$configs = array();
/* =============== SETTINGS =============== */
$configs[] = new admin_setting_configcheckbox("enable", get_string('enable', $modname), get_string('enable_help', $modname), 0);
/* Site identifiers defined by Resource Bridge */
$configs[] = new admin_setting_configtext("payloadtype", get_string('payloadtype', $modname), get_string('payloadtype_help', $modname), '');
$configs[] = new admin_setting_configtext("resourcetype", get_string('resourcetype', $modname), get_string('resourcetype_help', $modname), '');
$configs[] = new admin_setting_configtext("tokentype", get_string('tokentype', $modname), get_string('tokentype_help', $modname), '');
$configs[] = new admin_setting_configtext("supplierid", get_string('supplierid', $modname), get_string('supplierid_help', $modname), '');
$configs[] = new admin_setting_configtext("supplier", get_string('supplier', $modname), get_string('supplier_help', $modname), '');
$configs[] = new admin_setting_configtext("provisoURL", get_string('provisoURL', $modname), get_string('provisoURL_help', $modname),'');
/* Debug variables - set UW TermCode and UW SubjectCode */
$configs[] = new admin_setting_heading("Debug", "Debug settings", "Leave these fields blank, except when debugging");
$configs[] = new admin_setting_configtext("termcode", get_string('termcode',$modname), get_string('termcode_help',$modname), '' );
$configs[] = new admin_setting_configtext("subjectcode", get_string('subjectcode',$modname), get_string('subjectcode_help',$modname), '' );
/* =============== SETTINGS =============== */

foreach ($configs as $config) {
   $config->plugin = $modname;
   $settings->add($config);
}