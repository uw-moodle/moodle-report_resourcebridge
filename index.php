<?php
/******************************************************************************
* UW Resource Bridge Report - Feed
*
* Report that generates an XML feed for use by the Resource Bridge.
*
* Author: Mike Litzkow/Nick Koeppen
******************************************************************************/
require_once("../../config.php");
require_once($CFG->libdir.'/adminlib.php');
require_once('lib.php');

////  *** There is no access control on this report.  It is readable by anyone. ***


$term = optional_param('term', null, PARAM_INT);
$dept = optional_param('dept', null, PARAM_INT);

$config = get_config("report_resourcebridge");
if( !isset($term)  && isset($config->termcode) ) {
    $term = $config->termcode;
}
if( !isset($dept) && isset($config->subjectcode) ) {
    $dept = $config->subjectcode;
}
if (empty($config->enable)) {
    $settingspage = "$CFG->wwwroot/$CFG->admin/settings.php?section=reportresourcebridge";
    print_error('notenabled', 'report_resourcebridge', $settingspage."#admin-enable");
} else {
    send_raw_xml( $term, $dept );
}

// This function sends XML to the browser
function send_raw_xml( $term, $dept ) {
    // Wrap the XML in appropriate HTTP headers so the browser knows
    // we're sending XML, not HTML.
    header("Content-type: text/xml");
    header("Content-Disposition: inline; filename=\"feed.xml\"");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
    header("Pragma: public");

    echo get_raw_xml( $term, $dept );
}

?>