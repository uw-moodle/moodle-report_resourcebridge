<?PHP
/******************************************************************************
* UW Resource Bridge Report - Version Identifier
*
* Moodle code fragment to establish plugin version.
*
* Author: Mike Litzkow/Nick Koeppen
******************************************************************************/

$plugin->version  = 2015072100;
$plugin->requires = 2010090501;
$plugin->component = 'report_resourcebridge'; // Full name of the plugin (used for diagnostics)
